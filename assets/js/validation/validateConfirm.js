const validateConfirm = (password, confirm) =>{
  if(confirm.value=='')
    return false;
  return password.value === confirm.value;
};

export default validateConfirm;