const validateWebsite = (input) => {
  const exp = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;
  return input.value.match(exp) == null ? false : true;
}

export default validateWebsite;