const validatePassword = (input) => {
  const exp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,15}$/;
  return exp.test(input.value);
};

export default validatePassword;