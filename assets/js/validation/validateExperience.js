const validateExperience = (input) => {
  let experience;

  if (input.value == 0) {
    experience = 'Beginner';
  }
  else if (input.value == 50) {
    experience = 'Intermediate';
  } else if (input.value == 100) {
    experience = 'Expert';
  }

  return experience;
}

export default validateExperience;