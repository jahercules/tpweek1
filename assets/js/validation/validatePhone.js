const validatePhone = (input) => {
  const exp = /^\(?([0-9]{4})\)?[-. ]?([0-9]{4})$/;
  return exp.test(input.value);
};

export default validatePhone;