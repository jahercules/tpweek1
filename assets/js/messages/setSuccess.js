const setSuccess = (input) => {
  input.classList.remove('error');
  input.classList.add('success');
};

export default setSuccess;