const setError = (input, message) => {
  const formControl =  input.parentElement;
  const small = formControl.querySelector('small');
  
  small.innerText = message;
  input.classList.remove('success');
  input.classList.add('error');
}

export default setError;