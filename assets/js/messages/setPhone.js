const setPhone = (phone) => {
  //add event on phone number to put a mask
  phone.addEventListener('blur', (e) => {
    try {
      let value = e.target.value.replace(/\D/g, '').match(/(\d{4})(\d{4})/);
      e.target.value = `${value[1]}-${value[2]}`;
    }
    catch (error) {}
  });
};

export default setPhone;