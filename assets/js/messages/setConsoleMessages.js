const setConsoleMessages = (data) => {
  data.forEach( value => console.log(`${value.id}: ${value.value}\n`));
};

export default setConsoleMessages;