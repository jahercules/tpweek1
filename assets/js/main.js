import validateOnlyText from './validation/validationOnlyText.js';
import validateEmail from './validation/validateEmail.js';
import validatePhone from './validation/validatePhone.js';
import validatePassword from './validation/validatePassword.js';
import validateAge from './validation/validateAge.js';
import validateWebsite from './validation/validateWebsite.js';
import validateExperience from './validation/validateExperience.js';
import setPhone from './messages/setPhone.js';
import setError from './messages/setError.js';
import setSuccess from './messages/setSuccess.js';
import setConsoleMessages from './messages/setConsoleMessages.js';
import validateConfirm from './validation/validateConfirm.js';

const submitButton = document.getElementById('submitInfo');
const name = document.getElementById('name');
const lastName = document.getElementById('lastName');
const email = document.getElementById('mail');
const phone = document.getElementById('phone');
const password = document.getElementById('password');
const passwordConfirm = document.getElementById('passwordConfirm');
const age = document.getElementById('age');
const website = document.getElementById('website');
const experience = document.getElementById('experience');
const newlester = document.getElementById('newlester');

//set phone format to xxxx-xxxx
setPhone(phone);

//Add event to submit
submitButton.addEventListener('click', (e) => {
  e.preventDefault();
  validateOnlyText(name)== true ? setSuccess(name) : setError(name, 'Enter a valid name');
  validateOnlyText(lastName) == true ? setSuccess(lastName) : setError(lastName, 'Enter a valid Last Name');
  validateEmail(email) == true ? setSuccess(email) : setError(email, 'Enter a valid Email address');
  validatePhone(phone) == true ? setSuccess(phone) : setError(phone, 'Enter a valid phone number');
  validateAge(age) == true ? setSuccess(age) : setError(age, 'Please enter your age');
  validatePassword(password) == true ? setSuccess(password) : setError(password, 'The password must contain 8 characters: lowercase, uppercase and number');
  validateConfirm(password, passwordConfirm) == true ? setSuccess(passwordConfirm) : setError(passwordConfirm, 'Passwords must match')
  validateWebsite(website) == true ? setSuccess(website) : setError(website, 'Please enter a valid website url');

  //Add condition to display data in console
  if(validateOnlyText(name) && validateOnlyText(lastName) &&
    validateEmail(email) && validatePhone(phone) &&
    validateAge(age) && validatePassword(password) && 
    validateConfirm(password, passwordConfirm) && validateWebsite(website)){
      let experienceValue = {
        id: experience.id,
        value: validateExperience(experience)
      }
      let checkbox = {
        id: newlester.id,
        value: newlester.checked == true ? 'Checked' : 'Unchecked'
      }
      setConsoleMessages([name, lastName, email, phone, age, password, passwordConfirm, website, experienceValue, checkbox ]);
  }
})

