const pressed = [];
const secretCode = 'hesoyam';
const modal = document.querySelector('.modal');

window.addEventListener('keyup', (e) => {
  pressed.push(e.key);
  pressed.splice(-secretCode.length - 1, pressed.length - secretCode.length);
  if (pressed.join('').toLowerCase().includes(secretCode)) {
    modal.classList.add('open');
  }
});

//remove when click another zone
modal.addEventListener('click', (e) => {
  if (e.target.classList.contains('modal')) {
    modal.classList.remove('open');
  }
});

//remove when hitting escape
window.addEventListener('keyup', (e) => {
  if (e.key === 'Escape') {
    modal.classList.remove('open');
  }
});